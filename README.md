**r-programming**

The repository, which contains codes for the collection, processing, analysis and visualization of statistical data with the number of vacancies and summary, as well as unstructured data of vacancies, resume and interview.

Репозиторий, который содержит коды для сбора, обработки, анализа и визуализации статистических данных количество вакансий и резюме, а также неструктурированных данных вакансий, резюме и интервью.
Files / Файлы

# №1 Analizy and vizualize log file
 #
In the file "Analizy and vizualize log file" shows the code for the collection, processing and visualizing data from log files from the server to your site.

В файле "Analizy and vizualize log file" приведён код для сбора, обработки и визуализации данных из лог файлов с сервера вашего сайта.

# №2 Parsers #

In the file "Parsers" following is the code template for collection (parsing) data from job sites. The parsing is done in the console (Linux, OSX). First, we create the file with the url of the pages from which data will be collected, specify the XPath of the coordinates on the pages, the data that we collect. In conclusion, all the data will sohranim with the file. In the R console run the command - "R". Next, activate the desired packages, specify the directory folder where the files will be saved with the data then connect and run the file to collect the data. The first two options differ in the packages and code, but the main function is the same - to collect data. The third amended version of the API you connect to the site if necessary, but otherwise unchanged.

В файле "Parsers" приведен шаблон кода для сбора (парсинга) данных с работных сайтов. Парсинг осуществляется в консоле (Linux, OSX). Для начала создаём файл с url страниц, с которых будут собираться данные, указываем XPath координаты на страницах, данных которые будем собирать. В заключение все данные сохранем с файл. В консоле запускаем R, командой - "R". Далее активируем нужные пакеты, указываем директорию папки, в которую будут сохраняться файлы с данными затем подключаем и запускаем файл для сбора данных. Первые два варианта отличаются пакетами и кодом, но главная функция одинакова - собрать данные. Третий вариант дополнен подключением API к сайту если это необходимо, в остальном без изменений.

# №3 Semantic and vizualyze #

In "Semantic and vizualyze" is the code to process unstructured data (in this case data in text format). The code processes the data in Russian and English. However, the code can process data in other languages, you just need to connect additional libraries for processing and removal of stop words processed language. Code provodit quantitative content analysis forms the body of the word-forms text array of data that forms the matrix of word forms and visualizes the data.

В файле "Semantic and vizualyze" приводится код для обработки неструктурированных данных (в данном случае данных в текстовом формате). Код обрабатывает данные на русском и английском языке. Впрочем код может обработать данные и на других языках, просто необходимо подключить дополнительные библиотеки для обработки и удаления стоп-слов обрабатываемого языка. Код проводитт количественный контент анализ, формирует корпуса словоформ массива текстовых данных, фомирует матрицы словоформ, а так же визуализирует данные.